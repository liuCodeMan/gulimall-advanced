package com.atguigu.gulimall.ware.dao;

import com.atguigu.gulimall.ware.entity.UndoLogEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * 
 * @author liuyongxiang
 * @email 664732047@qq.com
 * @date 2022-01-03 09:22:02
 */
@Mapper
public interface UndoLogDao extends BaseMapper<UndoLogEntity> {
	
}
