package com.atguigu.gulimall.ware.dao;

import com.atguigu.gulimall.ware.entity.PurchaseEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 采购信息
 * 
 * @author liuyongxiang
 * @email 664732047@qq.com
 * @date 2022-01-03 09:22:02
 */
@Mapper
public interface PurchaseDao extends BaseMapper<PurchaseEntity> {
	
}
