package com.atguigu.common.to.es;

import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

@Data
public class SkuEsModel {

    private Long skuId;

    private Long spuId;

    private String skuTitle;

    private BigDecimal skuPrice;

    private String skuImg;
    //销量
    private Long saleCount;
    //是否拥有库存
    private Boolean hasStock;
    //热度评分
    private Long hotScore;
    //品牌ID
    private Long brandId;
    //分类ID
    private Long catalogId;
    //品牌名字
    private String brandName;
    //品牌图片
    private String brandImg;
    //分类的名字
    private String catalogName;

    private List<Attrs> attrs;
    @Data
    public static class Attrs{
        private Long attrId;
        private String attrName;
        private String attrValue;
    }


}
