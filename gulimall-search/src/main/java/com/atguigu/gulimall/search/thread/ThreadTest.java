package com.atguigu.gulimall.search.thread;

import java.util.concurrent.*;

public class ThreadTest {
    public static ExecutorService executor = Executors.newFixedThreadPool(10);

    public static void main(String[] args) throws ExecutionException, InterruptedException {
        System.out.println("main.......start......");
//        CompletableFuture<Void> future = CompletableFuture.runAsync(() -> {
//            System.out.println("当前线程：" + Thread.currentThread().getId());
//            int i = 10 / 2;
//            System.out.println("运行结果：" + i);
//        }, executor);
        /*CompletableFuture<Integer> future = CompletableFuture.supplyAsync(() -> {
            System.out.println("当前线程：" + Thread.currentThread().getId());
            Integer i = 10 / 0;
            System.out.println("运行结果：" + i);
            return i;
        }, executor).whenComplete((res,exception)->{
            System.out.println("异步任务成功完成了。。。结果是："+res+";异常是："+exception);
        }).exceptionally(throwable -> {
            return 10;
        });*/
        /*CompletableFuture<Integer> future = CompletableFuture.supplyAsync(() -> {
            System.out.println("当前线程：" + Thread.currentThread().getId());
            Integer i = 10 / 0;
            System.out.println("运行结果：" + i);
            return i;
        }, executor).handle((res, tre) -> {
            if (res != null) {
                return res * 2;
            }
            if (tre != null) {
                return 0;
            }
            return 0;
        });
        Integer integer = future.get();*/
        /*CompletableFuture<Void> future = CompletableFuture.supplyAsync(() -> {
            System.out.println("当前线程：" + Thread.currentThread().getId());
            int i = 10 / 4;
            System.out.println("运行结果：" + i);
            return i;
        }, executor).thenRunAsync(() -> {
            System.out.println("任务二执行了。。。");
        }, executor);*/
        /*CompletableFuture<Void> future = CompletableFuture.supplyAsync(() -> {
                    System.out.println("当前线程：" + Thread.currentThread().getId());
                    int i = 10 / 4;
                    System.out.println("运行结果：" + i);
                    return i;
                }, executor).thenAcceptAsync(a->{
            System.out.println("任务2启动了。。。" + a);
        },executor);*/
       /* CompletableFuture<String> future = CompletableFuture.supplyAsync(() -> {
            System.out.println("当前线程：" + Thread.currentThread().getId());
            int i = 10 / 4;
            System.out.println("运行结果：" + i);
            return i;
        }, executor).thenApplyAsync(a -> {
            System.out.println("任务二执行了。。。" + a);
            return "Hello " + a;
        }, executor);*/
        CompletableFuture<Integer> future01 = CompletableFuture.supplyAsync(() -> {
            System.out.println("当前01线程：" + Thread.currentThread().getId());
            int i = 10 / 4;
            System.out.println("运行01结果：" + i);
            return i;
        }, executor);
        CompletableFuture<String> future02 = CompletableFuture.supplyAsync(() -> {
            System.out.println("线程二执行");
            return "hello";
        }, executor);
        /*future01.runAfterBothAsync(future02,()->{
            System.out.println("线程3执行");
        },executor);*/
        /*future01.thenAcceptBothAsync(future02,(f1,f2)->{
            System.out.println("任务3执行--"+ f1 + "----->" + f2);
        },executor);
        System.out.println("main.......end........");*/
        CompletableFuture<String> future = future01.thenCombineAsync(future02, (f1, f2) -> {
            return f1 + f2 + "--->hhh";
        }, executor);
        System.out.println("main.......end........"+ future.get());
    }

    public  void main1(String[] args) throws ExecutionException, InterruptedException {
        System.out.println("main.......start......");

        /**
         * 1、继承thread用法
         * thread01 thread01 = new thread01();
         *         thread01.start();
         * 2、实现Runnable用法
         * Runable runable = new Runable();
         *         new Thread(runable).start();
         * 3、实现Callable的用法 FutureTask
         *  FutureTask<Integer> futureTask = new FutureTask<>(new Callable01());
         *         new Thread(futureTask).start();
         *         Integer integer = futureTask.get();
         */

        ThreadPoolExecutor executor = new ThreadPoolExecutor(5,
                200,
                10,
                TimeUnit.SECONDS,
                new LinkedBlockingDeque<>(100000),
                Executors.defaultThreadFactory(),
                new ThreadPoolExecutor.AbortPolicy());
        System.out.println("main.......end........");
    }

    public static class thread01 extends Thread{
        @Override
        public void run() {
            System.out.println("当前线程："+ Thread.currentThread().getId());
            int i = 10/2;
            System.out.println("运行结果：" + i);
        }
    }

    public static class Runable implements Runnable{

        @Override
        public void run() {
            System.out.println("当前线程："+ Thread.currentThread().getId());
            int i = 10/2;
            System.out.println("运行结果：" + i);
        }
    }

    public static class Callable01 implements Callable<Integer>{
        @Override
        public Integer call() throws Exception {
            System.out.println("当前线程："+ Thread.currentThread().getId());
            int i = 10/2;
            System.out.println("运行结果：" + i);
            return 1;
        }
    }
}
