package com.atguigu.gulimall.order.config;

import com.alipay.api.AlipayApiException;
import com.alipay.api.AlipayClient;
import com.alipay.api.DefaultAlipayClient;
import com.alipay.api.request.AlipayTradePagePayRequest;
import com.atguigu.gulimall.order.vo.PayVo;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@ConfigurationProperties(prefix = "alipay")
@Component
@Data
public class AlipayTemplate {

    //在支付宝创建的应用的id
    private   String app_id = "2021000119614440";
    private String timeout = "30m";

    // 商户私钥，您的PKCS8格式RSA2私钥
    private  String merchant_private_key = "MIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQCP5a4lBkUxtQp/4EOYLLfEISfhNL8reYDllZR+Ckt1sn0ytXgCtb9Yw98vBvFsvuiQNvMWa5DMghS7ZL0ptgMipDbPHAtC0WyRSxr9P4aWif/3ohk1ekROB6wYZk/jV6AGy+aqM0mEX1heTuI8QVfvzF8bXgsBlA4HENKfhnio1I623tcGhB3PUB0ZhsUwGw4ZJRqa6SlqL3earDT0vxAd7hfhapLW3DcTzhVnkptOT2WYEd48+fWlxm+LK07bq8ya+U4qmqRrVN5+CrmrIwyWAM69qFaUqS6iHKj7NCAvzHVsv20z8PmCTBEBh61VYUG8HnSTChjaN4A7sGjINiF/AgMBAAECggEAZH/5nIpAkXLsHlu3WfUBc83kWHnKesalq7NIvq7Z/R3oKx5q7xmYxpomqmWjAFxleTOnm6UMwIipiifVpeglUHB4hzYC92vk86UM+IsBqOpBB+cCzELUqdrZ60Cp0xJEBfsq4vxnADjHMzbM82B/1k9arXaFIVkSYwTw1dHJO8AMwyvA3/AznyEKOJ+tG85rqJDsbhhYCQ21Ks0ZoXUsaeqZfLfQ46An3I2HCLRZW3c2jWrYmSZH/8xNNxxBX/3zr6HRQPFPwmzgHh8CX6/IF3WjdZqmBAcjGYGngMyWntl6cdO16AHBfqKeRtf7EpJ5G5INizGkyr5Vi947teQKwQKBgQDAXEXKdLRdQQqS/Ws4XBlr+S0xkQCN8XLMX0xQZZSMej6C2FkoWJgUUMazwUwQ3mZbcVhXUoqGkI1gnI94g6OeKqrRkM/X7WwKBvsTp8guShsABK9xHeK+yB13HVEDXVJTcG37TGGTqlMlOBbbkD3tMYkl9avYKaYQnOY+CBUc2QKBgQC/gN9/XFwAz6kbzse8Xvt762n0uOVrKcb+ASxHXGlHZbi8TTo8RrvOICHfNa6UjNsuJNZxQoqhrRphiCkF0Ls5b85WGxvkec/ZTZD/zFpmV+bR/jkx5K5eqpv4bLr1Ykg8i3aENMoSDymvteXEqxaxJiOEorJ7gL3/ug/m8OaaFwKBgQCfaRhxMgvyqB8CNz8uwqxSnXnFdNwXxXsUQPl2OuhDDl9VgcNJOzo0w8gXWV4XMKd0qlbCbuXbpFl1Mel3vufOAJ/08eQ8lkYNfUF/RXEQY1tCxZ86x8CLyD0a5FGpzyGVgjuyoNbQT7t9waC3ri6TrZRxv8j8Z292nv/zcjFE6QKBgGlMr5gUbPJ1PQNxXBqUD6w/8e8CjAO04RpiArrqcWk04bOIs3DZZAMfYACgP/Km0Q46az+z2w6zZEIk4Im2EHywocBu/5NN96ABKq287ChCRUVKiKCKoeaj4UdkYhjzhczk0rI3ZLUxaTaYylKt0LhvxC/2VjXawJ6Q7JWL+QDZAoGBAIe2uAy6z9N+n7FdvjR5LTS7ER2EMHR1W9guYhf2/PbogZVYtl8gsLqN7lyPBiygs8UhglE6nwq0/5QsMcubfXKsOOqro99wEzut+pYMCs8W7VJlCGE4sg7DtgO+VXftMoxyRK6Emg9DqBiaWnLFLlpfq90VvdIaTrRzEZ4jnI/v";
    // 支付宝公钥,查看地址：https://openhome.alipay.com/platform/keyManage.htm 对应APPID下的支付宝公钥。
    private  String alipay_public_key = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA6LITG9eJioeRqzkX3ejKMIMSYsoyT9vuJu6KM3poOpiUuzMMYaTjygdc85+OMPpWBBGJjwTkdRzfI9y6CfCbT0eoLzc0XDs2z36N/bIBXHcxuJg9hCU0ji5AA+Goj2Qq4tD50tiFv8jkqV0X+ees0dqM4VAki7kqOyjosnJ+PCDqvVto0KykYD4j9wyr+NGqnp/s+VQGkRo7C0sYSZN/1/BFb1masoxfjwUdRYzAEQqfhbB1uispl0gvVEXkZ0MQ/oHgu4SBh2bGMdzZR5aKL5QUwecWw8WSJzeSqqxAwTW/9QeTSJSUQRy4LBvmAmUo7yKPmdfcV9sIeJVSwkiHhwIDAQAB";
    // 服务器[异步通知]页面路径  需http://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问
    // 支付宝会悄悄的给我们发送一个请求，告诉我们支付成功的信息
    private  String notify_url = "http://liu.free.idcfengye.com/payed/notify";

    // 页面跳转同步通知页面路径 需http://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问
    //同步通知，支付成功，一般跳转到成功页
    private  String return_url = "http://member.gulimall.com/memberOrder.html";

    // 签名方式
    private  String sign_type = "RSA2";

    // 字符编码格式
    private  String charset = "utf-8";

    // 支付宝网关； https://openapi.alipaydev.com/gateway.do
    private  String gatewayUrl = "https://openapi.alipaydev.com/gateway.do";

    public  String pay(PayVo vo) throws AlipayApiException {

        //AlipayClient alipayClient = new DefaultAlipayClient(AlipayTemplate.gatewayUrl, AlipayTemplate.app_id, AlipayTemplate.merchant_private_key, "json", AlipayTemplate.charset, AlipayTemplate.alipay_public_key, AlipayTemplate.sign_type);
        //1、根据支付宝的配置生成一个支付客户端
        AlipayClient alipayClient = new DefaultAlipayClient(gatewayUrl,
                app_id, merchant_private_key, "json",
                charset, alipay_public_key, sign_type);

        //2、创建一个支付请求 //设置请求参数
        AlipayTradePagePayRequest alipayRequest = new AlipayTradePagePayRequest();
        alipayRequest.setReturnUrl(return_url);
        alipayRequest.setNotifyUrl(notify_url);

        //商户订单号，商户网站订单系统中唯一订单号，必填
        String out_trade_no = vo.getOut_trade_no();
        //付款金额，必填
        String total_amount = vo.getTotal_amount();
        //订单名称，必填
        String subject = vo.getSubject();
        //商品描述，可空
        String body = vo.getBody();

        alipayRequest.setBizContent("{\"out_trade_no\":\""+ out_trade_no +"\","
                + "\"total_amount\":\""+ total_amount +"\","
                + "\"subject\":\""+ subject +"\","
                + "\"body\":\""+ body +"\","
                + "\"timeout_express\":\""+timeout+"\","
                + "\"product_code\":\"FAST_INSTANT_TRADE_PAY\"}");

        String result = alipayClient.pageExecute(alipayRequest).getBody();

        //会收到支付宝的响应，响应的是一个页面，只要浏览器显示这个页面，就会自动来到支付宝的收银台页面
        System.out.println("支付宝的响应："+result);

        return result;

    }
}
