package com.atguigu.gulimall.order;

import com.atguigu.gulimall.order.entity.OrderReturnReasonEntity;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.amqp.core.AmqpAdmin;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.connection.CorrelationData;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;
import java.util.Date;
import java.util.UUID;

@Slf4j
@SpringBootTest
public class GulimallOrderApplicationTests {

    @Resource
    AmqpAdmin amqpAdmin;

    @Resource
    RabbitTemplate rabbitTemplate;

    @Test
    public void sendMessageTest(){
        String msg = "Hello World";
        for (int i = 0; i < 10; i++) {
            OrderReturnReasonEntity orderReturnReasonEntity = new OrderReturnReasonEntity();
            orderReturnReasonEntity.setId(1L);
            orderReturnReasonEntity.setName("哈哈" + i);
            orderReturnReasonEntity.setCreateTime(new Date());
            rabbitTemplate.convertAndSend("hello-java-exchange","hello-java22",orderReturnReasonEntity,new CorrelationData(UUID.randomUUID().toString()));
            log.info("消息发送完成【{}】",orderReturnReasonEntity);
        }

    }

    @Test
    public void test(){
        log.info("[{}]","success");
    }

    @Test
    public void helloJavaExchange(){
        DirectExchange directExchange = new DirectExchange("hello-java-exchange", true, false);
        amqpAdmin.declareExchange(directExchange);
        log.info("创建成功【{}】","hello-java-exchange");
    }

    @Test
    public void queue(){
        //Queue(String name, boolean durable, boolean exclusive, boolean autoDelete, @Nullable Map<String, Object> arguments)
        Queue queue = new Queue("hello-java-queue", true, false, false);
        amqpAdmin.declareQueue(queue);
        log.info("队列创建成功[{}]","hello-java-queue");
    }

    @Test
    public void bind(){
        Binding binding = new Binding("hello-java-queue", Binding.DestinationType.QUEUE,"hello-java-exchange","hello-java",null);
        amqpAdmin.declareBinding(binding);
        log.info("绑定创建成功[{}]","hello-java");
    }


}
