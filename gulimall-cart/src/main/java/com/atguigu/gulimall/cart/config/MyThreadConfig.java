package com.atguigu.gulimall.cart.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

//@EnableConfigurationProperties(ThreadPoolConfigProperties.class)
@Configuration
public class MyThreadConfig {

    @Bean
    public ThreadPoolExecutor threadPoolExecutor(ThreadPoolConfigProperties pool){
        return new ThreadPoolExecutor(pool.getCoreSize(),//核心线程数
                pool.getMaxSize(),//最大线程数
                pool.getKeepAliveTime(),//多长时间关闭
                TimeUnit.SECONDS,//时间单位
                new LinkedBlockingDeque<>(100000),//阻塞队列的长度
                Executors.defaultThreadFactory(),//线程工厂，用的是默认的
                new ThreadPoolExecutor.AbortPolicy());//抛弃策略
    }
}
